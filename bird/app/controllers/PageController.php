<?php

/* 
 * David Chavez - 2014
 * 1569682
 * 
 * Permite que las variables de todas las vistas también esten disponibles
 *  para las páginas estáticas creadas con este controlador
 */

class PageController extends BaseController {
   
    public function renderAbout() {
        return View::make('about');
    }
    
    public function renderContact() {
        return View::make('contact');
    }
}
