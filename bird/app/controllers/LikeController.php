<?php

/* 
 * David Chavez - 2014
 * 1569682
 */

class LikeController extends BaseController {
    
    public function like() {
        $article = new Article;
        $id = Input::get('id');
        
        $article->likeArticle($id);
        
        return Response::json(array('likes' => $article->getArticle($id)[0]->likes));
    }
    
    public function dislike() {
        $article = new Article;
        $id = Input::get('id');
        
        $article->dislikeArticle($id);
        
        return Response::json(array('dislikes' => $article->getArticle($id)[0]->dislikes));
    }
}
