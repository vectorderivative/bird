<?php

/* 
 * David Chavez - 2014
 * 1569682
 */

class SectionController extends BaseController {
    
    //Render lista de secciones con algunos artículos... alias, el home.
    public function render() {
        return Redirect::to('/');
    }
    
    public function showSection($id) {
        $articles = new Article;
        $section = new Section;
        
        return View::make('section', array(
            'section' => $section->getSection($id)[0],
            'articles' => $articles->getSectionArticles($id)
        ));
    }
    
    public function sortBy() {
        $articles = new Article;
        $section = new Section;
        
        if (Input::has('sort') && Input::has('id')) {
            switch (Input::get('sort')) {
                case 'date':
                    $articles = $articles->getSectionArticles(Input::get('id'));
                break;
                case 'views':
                    $articles = $articles->getSectionArticlesByViews(Input::get('id'));
                break;
                case 'comments':
                    $articles = $articles->getSectionArticlesByComments(Input::get('id'));
                break;
                case 'likes':
                    $articles = $articles->getSectionArticlesByLikes(Input::get('id'));
                break;
            }
            
            $sectionId = Input::get('id');
        } else {
            switch (func_get_arg(1)) {
                case 'date':
                    $articles = $articles->getSectionArticles(func_get_arg(0));
                break;
                case 'views':
                    $articles = $articles->getSectionArticlesByViews(func_get_arg(0));
                break;
                case 'comments':
                    $articles = $articles->getSectionArticlesByComments(func_get_arg(0));
                break;
                case 'likes':
                    $articles = $articles->getSectionArticlesByLikes(func_get_arg(0));
                break;
            }
            $sectionId = func_get_arg(0);
        }
        
        
        return View::make('section', array(
            'section' => $section->getSection($sectionId)[0],
            'articles' => $articles
        ));
    }
    
}
