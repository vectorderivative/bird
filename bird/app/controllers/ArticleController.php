<?php

/* 
 * David Chavez - 2014
 * 1569682
 */

class ArticleController extends BaseController {
    
    public function showArticle($id) {
        $article = new Article;
        $comments = new Comment;
        
        //Marcar un vista más
        $article->readArticle($id);
        
        return View::make('article', array(
            'article' => $article->getArticle($id)[0],
            'comments' => $comments->getArticleApprovedComments($id)
                ));
    }
}