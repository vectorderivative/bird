<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
                        setlocale(LC_ALL,"es_ES");
			$this->layout = View::make($this->layout);
		}
	}
        
        /*
         * throw variables to the views globally
         */
        public function __construct() {
            //Lista de sections
            $sections = new Section;
            View::share('mySectionsList', $sections->getSections());
        }

}
