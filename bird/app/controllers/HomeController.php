<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
        
        public function render() {
            $articlesBySection;
            $articles = new Article;
            $sections = new Section;
            $sections = $sections->getSections();
            
            if (count($sections)>0){
                $c=0;
                foreach($sections as $section) {
                    $articlesBySection[$c]['section_id'] = $section->id;
                    $articlesBySection[$c]['section_name'] = $section->name;
                    $articlesBySection[$c]['articles'] = $articles->getSectionXArticles($section->id,2);
                    $c++;
                }
            }
            
            return View::make('home', array(
                'sliderArticles' => $articles->getSliderArticles(),
                'sections' => $articlesBySection
            ));
        }

}
