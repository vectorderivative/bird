<?php

/* 
 * David Chavez - 2014
 * 1569682
 */

class AdminController extends BaseController {
    public function render() {
        View::make('admin');
    }
}
