<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('title',35);
                    $table->text('content');
                    $table->string('description',255);
                    $table->integer('likes')->default(0);
                    $table->integer('dislikes')->default(0);
                    $table->integer('views')->default(0);
                    $table->boolean('active')->default(1);
                    $table->boolean('published')->default(0);
                    $table->boolean('important')->default(0);
                    $table->timestamps(); //Agrega created_at & updated_at
                    $table->softDeletes();
                    
                    $table->unsignedInteger('thumbnail')->nullable();
                    $table->unsignedInteger('bigPicture')->nullable();
                    $table->unsignedInteger('section_id');
                    $table->unsignedInteger('user_id');
                    //FK
                    $table->foreign('thumbnail')->references('id')->on('images');
                    $table->foreign('bigPicture')->references('id')->on('images');
                    $table->foreign('section_id')->references('id')->on('sections');
                    $table->foreign('user_id')->references('id')->on('users');
                });
                
                DB::transaction(function () {
                    $addArticle = <<<SQL
                                CREATE PROCEDURE `addArticle`(
                                    IN `title` VARCHAR(35),
                                    IN `content` TEXT,
                                    IN `description` VARCHAR(255),
                                    IN `thumbnail` INT(11),
                                    IN `section_id` INT(11),
                                    IN `user_id` INT(10))
                                    BEGIN
                                        INSERT INTO `articles` (
                                            `title`,
                                            `content`,
                                            `description`,
                                            `thumbnail`,
                                            `created_at`,
                                            `section_id`,
                                            `user_id`)
                                        VALUES
                                            (title,
                                             content,
                                             description,
                                             thumbnail,
                                             null,
                                             section_id,
                                             user_id);
                            END
SQL;
                    $deleteArticle = <<<SQL
                                CREATE PROCEDURE `deleteArticle`(
                                    IN `article_id` INT(10))
                                    BEGIN
                                        UPDATE `articles` 
                                        SET `active` = 0
                                        WHERE `id` = article_id;
                                    END
SQL;
                    $updateArticle = <<<SQL
                                CREATE PROCEDURE `updateArticle`(
                                    IN `article_id` INT(10),
                                    IN `title` VARCHAR(35),
                                    IN `content` TEXT,
                                    IN `description` VARCHAR(255),
                                    IN `thumbnail` INT(10),
                                    IN `section_id` INT(10),
                                    IN `user_id` INT(10))
                                BEGIN
                                    UPDATE `articles` 
                                    SET 
                                    `title` = title,
                                    `content` = content,
                                    `description` = description,
                                    `thumbnail` = thumbnail,
                                    `section_id` = section_id,
                                    `user_id` = user_id
                                WHERE `id` = article_id;
                                END
SQL;
                    $getArticle = <<<SQL
                                CREATE PROCEDURE `getArticle`(
                                    IN `article_id` INT(10))
                                        BEGIN
                                            SELECT
                                                `articles`.`id`,
                                                `articles`.`title`,
                                                `articles`.`content`,
                                                `articles`.`description`,
                                                `articles`.`thumbnail`,
                                                `articles`.`likes`,
                                                `articles`.`dislikes`,
                                                `articles`.`important`,
                                                `articles`.`created_at`,
                                                `articles`.`updated_at`,
                                                `articles`.`section_id`,
                                                `sections`.`name` as section_name,
                                                `articles`.`user_id`,
                                                `users`.`name` as user_name,
                                                `users`.`lastname` as user_lastname
                                                    FROM 
                                                        `articles`, `users`, `sections`
                                                    WHERE 
                                                        `sections`.`id` = `articles`.`section_id` 
                                                        AND
                                                        `users`.`id` = `articles`.`user_id`
                                                        AND
                                                        `articles`.`id` = article_id
                                                        AND
                                                        `articles`.`active` = 1;
                                        END
SQL;
                    $getArticles = <<<SQL
                                CREATE PROCEDURE `getArticles`()
                                BEGIN
                                    SELECT
                                        `articles`.`id`,
                                        `articles`.`title`,
                                        `articles`.`description`,
                                        `articles`.`thumbnail`,
                                        `articles`.`likes`,
                                        `articles`.`dislikes`,
                                        `articles`.`important`,
                                        `articles`.`created_at`,
                                        `articles`.`updated_at`,
                                        `articles`.`section_id`,
                                        `sections`.`name` as section_name,
                                        `articles`.`user_id`,
                                        `users`.`name` as user_name,
                                        `users`.`lastname` as user_lastname
                                    FROM `articles`, `users`, `sections`
                                    WHERE 
                                        `sections`.`id` = `articles`.`section_id` 
                                        AND
                                        `users`.`id` = `articles`.`user_id`
                                        AND
                                        `articles`.`active` = 1;
                                END
SQL;
                    $getSliderArticles = <<<SQL
                                CREATE PROCEDURE `getSliderArticles`()
                                    BEGIN
                                        SELECT
                                            `articles`.`id`,
                                            `articles`.`title`,
                                            `articles`.`description`,
                                            `articles`.`bigPicture`,
                                            `articles`.`created_at`,
                                            `articles`.`section_id`,
                                            `sections`.`name` as section_name,
                                            `articles`.`user_id`,
                                            `users`.`name` as user_name,
                                            `users`.`lastname` as user_lastname
                                        FROM `articles`, `users`, `sections`
                                        WHERE 
                                            `sections`.`id` = `articles`.`section_id` 
                                            AND
                                            `users`.`id` = `articles`.`user_id`
                                            AND
                                            `articles`.`active` = 1
                                            AND 
                                            `articles`.`bigPicture` IS NOT NULL
                                        ORDER BY `created_at` DESC LIMIT 5;
                                    END
SQL;
                    $getSectionArticles = <<<SQL
                                CREATE PROCEDURE `getSectionArticles`(
                                    IN `section_id` INT(10))
                                        BEGIN
                                            SELECT 
                                                `articles`.`id`,
                                                `articles`.`title`,
                                                `articles`.`description`,
                                                `articles`.`thumbnail`,
                                                `articles`.`likes`,
                                                `articles`.`dislikes`,
                                                `articles`.`created_at`,
                                                `articles`.`user_id`,
                                                `users`.`name` as user_name,
                                                `users`.`lastname` as user_lastname
                                            FROM `articles`, `users`
                                            WHERE 
                                                `articles`.`section_id` = section_id
                                                AND
                                                `articles`.`user_id` = `users`.`id`
                                                AND
                                                `articles`.`active` = 1
                                            ORDER BY `articles`.`created_at` DESC;
                                        END
SQL;
                    $getSectionXArticles = <<<SQL
                                CREATE PROCEDURE `getSectionXArticles`(
                                    IN `section_id` INT(10),
                                    IN `limite` INT(2))
                                        BEGIN
                                            SELECT 
                                                `articles`.`id`,
                                                `articles`.`title`,
                                                `articles`.`description`,
                                                `articles`.`thumbnail`,
                                                `articles`.`likes`,
                                                `articles`.`dislikes`,
                                                `articles`.`created_at`,
                                                `articles`.`user_id`,
                                                `users`.`name` as user_name,
                                                `users`.`lastname` as user_lastname
                                            FROM `articles`, `users`
                                            WHERE 
                                                `articles`.`section_id` = section_id
                                                AND
                                                `articles`.`user_id` = `users`.`id`
                                                AND
                                                `articles`.`active` = 1
                                            ORDER BY `articles`.`created_at` DESC
                                            LIMIT limite;
                                        END
SQL;
                    $getSectionArticlesByLikes = <<<SQL
                                CREATE PROCEDURE `getSectionArticlesByLikes`(
                                    IN `section_id` INT(10))
                                BEGIN
                                    SELECT 
                                        `articles`.`id`,
                                        `articles`.`title`,
                                        `articles`.`description`,
                                        `articles`.`thumbnail`,
                                        `articles`.`likes`,
                                        `articles`.`dislikes`,
                                        `articles`.`created_at`,
                                        `articles`.`user_id`,
                                        `users`.`name` as user_name,
                                        `users`.`lastname` as user_lastname
                                    FROM `articles`, `users`
                                    WHERE 
                                        `articles`.`section_id` = section_id
                                        AND
                                        `articles`.`user_id` = `users`.`id`
                                        AND
                                        `articles`.`active` = 1
                                    ORDER BY `articles`.`likes` DESC;
                                END
SQL;
                    $getSectionArticlesByViews = <<<SQL
                                CREATE PROCEDURE `getSectionArticlesByViews`(
                                    IN `section_id` INT(10))
                                BEGIN
                                    SELECT 
                                        `articles`.`id`,
                                        `articles`.`title`,
                                        `articles`.`description`,
                                        `articles`.`thumbnail`,
                                        `articles`.`likes`,
                                        `articles`.`dislikes`,
                                        `articles`.`created_at`,
                                        `articles`.`user_id`,
                                        `users`.`name` as user_name,
                                        `users`.`lastname` as user_lastname
                                    FROM `articles`, `users`
                                    WHERE 
                                        `articles`.`section_id` = section_id
                                        AND
                                        `articles`.`user_id` = `users`.`id`
                                        AND
                                        `articles`.`active` = 1
                                    ORDER BY `articles`.`views` DESC;
                                END
SQL;
                    $getSectionArticlesByComments = <<<SQL
                                CREATE PROCEDURE `getSectionArticlesByComments`(
                                    IN `section_id` INT(10))
                                BEGIN
                                    SELECT 
                                        IFNULL(COUNT(`comments`.`id`),0) AS contar,
                                        `articles`.`id`,
                                        `articles`.`title`,
                                        `articles`.`description`,
                                        `articles`.`thumbnail`,
                                        `articles`.`likes`,
                                        `articles`.`dislikes`,
                                        `articles`.`created_at`,
                                        `articles`.`user_id`,
                                        `users`.`name` as user_name,
                                        `users`.`lastname` as user_lastname
                                    FROM `users`, `comments` 
                                    RIGHT JOIN `articles` ON `comments`.`article_id` = `articles`.`id`
                                    WHERE 
                                        `articles`.`section_id` = section_id
                                        AND
                                        `articles`.`user_id` = `users`.`id`
                                        AND
                                        `articles`.`active` = 1
                                    GROUP BY `articles`.`id`
                                    ORDER BY contar DESC;
                                END
SQL;
                    $getUserArticles = <<<SQL
                                CREATE PROCEDURE `getUserArticles`(
                                    IN `user_id` INT(10))
                                    BEGIN
                                        SELECT
                                            `articles`.`id`,
                                            `articles`.`title`,
                                            `articles`.`description`,
                                            `articles`.`thumbnail`,
                                            `articles`.`likes`,
                                            `articles`.`dislikes`,
                                            `articles`.`created_at`,
                                            `articles`.`updated_at`,
                                            `articles`.`section_id`,
                                            `sections`.`name` as section_name,
                                            `articles`.`user_id`,
                                            `users`.`name` as user_name,
                                            `users`.`lastname` as user_lastname
                                        FROM `articles`, `users`, `sections`
                                        WHERE 
                                            `articles`.`user_id` = user_id
                                            AND `sections`.`id` = `articles`.`section_id`
                                            AND `users`.`id` = user_id
                                            AND
                                            `articles`.`active` = 1;
                                    END
SQL;
                    $likeArticle = <<<SQL
                                CREATE PROCEDURE `likeArticle`(
                                    IN `article_id` INT(10))
                                    BEGIN
                                        UPDATE `articles`
                                        SET `likes` = `likes`+1
                                        WHERE `id` = article_id;
                                    END
SQL;
                    $dislikeArticle = <<<SQL
                                CREATE PROCEDURE `dislikeArticle`(
                                    IN `article_id` INT(10))
                                        BEGIN
                                            UPDATE `articles`
                                            SET `dislikes` = `dislikes`+1
                                            WHERE `id` = article_id;
                                        END
SQL;
                    $readArticle = <<<SQL
                                CREATE PROCEDURE `readArticle`(
                                IN `article_id` INT(10))
                                BEGIN
                                    UPDATE `articles`
                                    SET `views` = `views`+1
                                    WHERE `id` = article_id;
                                END
SQL;

                    DB::unprepared($addArticle);
                    DB::unprepared($deleteArticle);
                    DB::unprepared($dislikeArticle);
                    DB::unprepared($getArticle);
                    DB::unprepared($getArticles);
                    DB::unprepared($getSectionArticles);
                    DB::unprepared($getSectionArticlesByComments);
                    DB::unprepared($getSectionArticlesByLikes);
                    DB::unprepared($getSectionArticlesByViews);
                    DB::unprepared($getSectionXArticles);
                    DB::unprepared($getSliderArticles);
                    DB::unprepared($getUserArticles);
                    DB::unprepared($likeArticle);
                    DB::unprepared($readArticle);
                    DB::unprepared($updateArticle);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('articles');
                DB::unprepared('DROP PROCEDURE addArticle; '
                        . 'DROP PROCEDURE deleteArticle; '
                        . 'DROP PROCEDURE dislikeArticle; '
                        . 'DROP PROCEDURE getArticle; '
                        . 'DROP PROCEDURE getArticles; '
                        . 'DROP PROCEDURE getSectionArticles; '
                        . 'DROP PROCEDURE getSectionArticlesByComments; '
                        . 'DROP PROCEDURE getSectionArticlesByLikes; '
                        . 'DROP PROCEDURE getSectionArticlesByViews; '
                        . 'DROP PROCEDURE getSectionXArticles; '
                        . 'DROP PROCEDURE getSliderArticles; '
                        . 'DROP PROCEDURE getUserArticles; '
                        . 'DROP PROCEDURE likeArticle; '
                        . 'DROP PROCEDURE readArticle; '
                        . 'DROP PROCEDURE updateArticle;');
	}

}
