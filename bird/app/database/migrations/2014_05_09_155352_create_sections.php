<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSections extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sections', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('name',20)->unique();
                    $table->string('description',255);
                    $table->tinyinteger('position')->default(0);
                    $table->boolean('active')->default(1);
                    $table->timestamps();
                    $table->softDeletes();
                });
                
                DB::transaction(function () {
                    //Trigger
                    $SQLTrigger = <<<SQL
                                CREATE TRIGGER onDeleteSection AFTER UPDATE ON sections 
                                    FOR EACH ROW
                                        IF (NEW.active <> OLD.active AND NEW.active = 0) THEN
                                            UPDATE `articles` SET `active`= 0 WHERE section_id = NEW.id;
                                        END IF
SQL;
                    DB::unprepared($SQLTrigger);

                    //Stores procedures
                    $addSection = <<<SQL
                                CREATE PROCEDURE `addSection`(
                                    IN `name` VARCHAR(20),
                                    IN `description` VARCHAR(255),
                                    IN `position` TINYINT)
                                BEGIN
                                    INSERT INTO `sections` 
                                        (`name`,`description`,`position`,`created_at`)
                                        VALUES
                                        (name, description, position, null);
                                END
SQL;
                    $deleteSection = <<<SQL
                                CREATE PROCEDURE `deleteSection`(
                                    IN `section_id` INT(10))
                                        BEGIN
                                            UPDATE `sections`
                                            SET `active` = 0
                                            WHERE `id` = section_id;
                                        END
SQL;
                    $updateSection = <<<SQL
                                CREATE PROCEDURE `updateSection`(
                                    IN `section_id` INT(10),
                                    IN `name` VARCHAR(20),
                                    IN `description` VARCHAR(255),
                                    IN `position` TINYINT)
                                BEGIN
                                    UPDATE `sections` 
                                    SET 
                                        `name` = name,
                                        `description` = description,
                                        `position` = position
                                    WHERE `id` = section_id;
                                END
SQL;
                    $getSection = <<<SQL
                                CREATE PROCEDURE `getSection`(
                                    IN `section_id` INT(10))
                                        BEGIN
                                            SELECT
                                                `id`,
                                                `name`,
                                                `description`,
                                                `position`,
                                                `created_at`,
                                                `updated_at`
                                            FROM `sections`
                                            WHERE `id` = section_id
                                            AND `active` = 1;
                                END
SQL;
                    $getSections = <<<SQL
                                CREATE PROCEDURE `getSections`()
                                    BEGIN
                                        SELECT 
                                            `id`,
                                            `name`,
                                            `description`,
                                            `position`,
                                            `created_at`,
                                            `updated_at`
                                        FROM `sections`
                                        WHERE `active` = 1
                                        ORDER BY `position` ASC;
                                    END
SQL;

                    DB::unprepared($addSection);
                    DB::unprepared($deleteSection);
                    DB::unprepared($getSection);
                    DB::unprepared($getSections);
                    DB::unprepared($updateSection);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                DB::unprepared('DROP TRIGGER onDeleteSection');
		Schema::dropIfExists('sections');
                DB::unprepared('DROP PROCEDURE addSection; DROP PROCEDURE deleteSection; DROP PROCEDURE getSection; DROP PROCEDURE getSections; DROP PROCEDURE updateSection;');
	}

}
