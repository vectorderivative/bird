<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_permissions', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->unsignedInteger('user_id');
                    $table->unsignedInteger('permission_id');
                    $table->timestamps();
                    //FK
                    $table->foreign('user_id')->references('id')->on('users');
                    $table->foreign('permission_id')->references('id')->on('permissions');
                });
                
                DB::transaction(function () {
                    $setPermission = <<<SQL
                                CREATE 
                                    PROCEDURE `setPermission`(
                                        IN `user_id` INT(10),
                                        IN `permission_id` INT(10))
                                BEGIN
                                    INSERT INTO `user_permissions`
                                        (`user_id`, `permission_id`)
                                    VALUES
                                        (user_id, permission_id);
                                END
SQL;
                    $unsetPermission = <<<SQL
                                CREATE PROCEDURE `unsetPermission`(
                                    IN `id` INT(10))
                                BEGIN
                                    DELETE FROM `user_permissions`
                                    WHERE `user_permission_id` = id;
                                END
SQL;
                    $getUserPermissions = <<<SQL
                                CREATE PROCEDURE `getUserPermissions`(
                                    IN `id` INT(10))
                                BEGIN
                                    SELECT
                                        `permissions`.`id`,
                                        `permissions`.`name`,
                                        `permissions`.`access`,
                                        `permissions`.`description`,
                                        `users`.`id`,
                                        `users`.`username`
                                    FROM `permissions`, `user_permissions`, `users`
                                    WHERE `users`.`id` = id
                                    AND `user_permissions`.`user_id` = `users`.`id`
                                    AND `user_permissions`.`permission_id` = `permissions`.`id`;
                                END
SQL;
                    $getUsersPermissions = <<<SQL
                                CREATE PROCEDURE `getUsersPermissions`()
                                    BEGIN
                                        SELECT
                                            `permissions`.`id`,
                                            `permissions`.`name`,
                                            `permissions`.`access`,
                                            `permissions`.`description`,
                                            `users`.`id`,
                                            `users`.`username`
                                        FROM 
                                            `users` 
                                        JOIN 
                                            `user_permissions` ON `users`.`id` = `user_permissions`.`user_id`
                                        JOIN 
                                            `permissions` ON `user_permissions`.`permission_id` = `permissions`.`id`
                                        WHERE 
                                            `users`.`active` = 1;
                                END
SQL;
                    DB::unprepared($getUserPermissions);
                    DB::unprepared($getUsersPermissions);
                    DB::unprepared($setPermission);
                    DB::unprepared($unsetPermission);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user_permissions');
                DB::unprepared('DROP PROCEDURE getUserPermissions;'
                        . ' DROP PROCEDURE getUsersPermissions;'
                        . ' DROP PROCEDURE setPermission;'
                        . ' DROP PROCEDURE unsetPermission;');
	}

}
