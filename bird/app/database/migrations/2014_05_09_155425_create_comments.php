<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function($table) {
                   $table->engine = 'InnoDB';
                   $table->increments('id');
                   $table->text('comment');
                   $table->string('username')->nullable();
                   $table->string('email')->nullable();
                   $table->boolean('approved')->default(0);
                   $table->boolean('active')->default(1);
                   $table->timestamps();
                   $table->softDeletes();
                   
                   $table->unsignedInteger('article_id');
                   $table->unsignedInteger('user_id')->nullable();
                   $table->unsignedInteger('comment_parent')->nullable();
                   //FK
                   $table->foreign('article_id')->references('id')->on('articles');
                   $table->foreign('user_id')->references('id')->on('users');
                   $table->foreign('comment_parent')->references('id')->on('comments');
                });
                
                DB::transaction(function () {  
                    $addComment = <<<SQL
                                CREATE PROCEDURE `addComment`(
                                    IN `comment` TEXT,
                                    IN `article_id` INT(10),
                                    IN `user_id` INT(10),
                                    IN `comment_parent` INT(10),
                                    IN `username` VARCHAR(20),
                                    IN `email` VARCHAR(50))
                                BEGIN
                                    INSERT INTO `comments` (
                                        `comment`,
                                        `article_id`,
                                        `user_id`,
                                        `comment_parent`,
                                        `username`,
                                        `email`,
                                        `created_at`)
                                    VALUES (
                                        comment, 
                                        article_id, 
                                        IF (user_id=0, null, user_id), 
                                        IF (comment_parent=0, null, comment_parent), 
                                        username, 
                                        email, 
                                        null);
                                END
SQL;
                    $deleteComment = <<<SQL
                                CREATE PROCEDURE `deleteComment`(
                                    IN `comment_id` INT(10))
                                BEGIN
                                    UPDATE `comments`
                                    SET `active` = 0
                                    WHERE `id` = comment_id;
                                END
SQL;
                    $updateComment = <<<SQL
                                CREATE PROCEDURE `updateComment`(
                                    IN `comment_id` INT(10),
                                    IN `comment` TEXT)
                                BEGIN
                                    UPDATE `comments`
                                    SET 
                                    `comment` = comment
                                    WHERE `id` = comment_id;
                                END
SQL;
                    $approveComment = <<<SQL
                            CREATE PROCEDURE `approveComment`(
                                IN `comment_id` INT(10))
                            BEGIN
                                UPDATE `comments`
                                SET `approved` = 1
                                WHERE `id` = comment_id;
                            END
SQL;
                    $unapproveComment = <<<SQL
                            CREATE PROCEDURE `unapproveComment`(
                                IN `comment_id` INT(10))
                            BEGIN
                                UPDATE `comments`
                                SET `approved` = 0
                                WHERE `id` = comment_id;
                            END
SQL;
                    $getComment = <<<SQL
                                CREATE PROCEDURE `getComment`(
                                    IN `comment_id` INT(10))
                                BEGIN
                                    SELECT
                                        `id`,
                                        `comment`,
                                        `created_at`,
                                        `updated_at`,
                                        `article_id`,
                                        `user_id`,
                                        `comment_parent`,
                                        `username`,
                                        `email`
                                    FROM `comments`
                                    WHERE `id` = comment_id;
                                END
SQL;
                    $getArticleComments = <<<SQL
                                CREATE PROCEDURE `getArticleComments`(
                                    IN `article_id` INT(10))
                                BEGIN
                                    SELECT 
                                        `comments`.`id`,
                                        `comments`.`comment`,
                                        `comments`.`created_at`,
                                        `comments`.`updated_at`,
                                        `comments`.`user_id`,
                                        `comments`.`approved`,
                                        `users`.`username` AS user_username,
                                        `users`.`name` AS user_name,
                                        `users`.`lastname` AS user_lastname,
                                        `users`.`avatar` AS user_avatar, 
                                        `comments`.`comment_parent`,
                                        `comments`.`username`
                                     FROM 
                                        `comments`, `users`
                                     WHERE 
                                         IF (`comments`.`user_id` IS NULL,
                                            `comments`.`article_id` = article_id,
                                            `comments`.`article_id` = article_id 
                                                AND 
                                            `comments`.`user_id` = `users`.`id`
                                        )
                                        AND `comments`.`active` = 1
                                     GROUP BY `comments`.`id`;
                                END
SQL;
                    $getArticleApprovedComments = <<<SQL
                    CREATE PROCEDURE `getArticleApprovedComments`(
                                    IN `article_id` INT(10))
                                BEGIN
                                    SELECT 
                                        `comments`.`id`,
                                        `comments`.`comment`,
                                        `comments`.`created_at`,
                                        `comments`.`updated_at`,
                                        `comments`.`user_id`,
                                        `users`.`username` AS user_username,
                                        `users`.`name` AS user_name,
                                        `users`.`lastname` AS user_lastname,
                                        `users`.`avatar` AS user_avatar, 
                                        `comments`.`comment_parent`,
                                        `comments`.`username`
                                     FROM 
                                        `comments`, `users`
                                     WHERE 
                                         IF (`comments`.`user_id` IS NULL,
                                            `comments`.`article_id` = article_id,
                                            `comments`.`article_id` = article_id 
                                                AND 
                                            `comments`.`user_id` = `users`.`id`
                                        )
                                        AND `comments`.`active` = 1
                                        AND `comments`.`approved` = 1
                                     GROUP BY `comments`.`id`;
                                END
SQL;
                                         
                    DB::unprepared($addComment);
                    DB::unprepared($deleteComment);
                    DB::unprepared($getArticleComments);
                    DB::unprepared($getComment);
                    DB::unprepared($updateComment);
                    DB::unprepared($approveComment);
                    DB::unprepared($getArticleApprovedComments);
                    DB::unprepared($unapproveComment);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('comments');
                DB::unprepared('DROP PROCEDURE addComment;'
                        . ' DROP PROCEDURE deleteComment;'
                        . ' DROP PROCEDURE getArticleComments;'
                        . ' DROP PROCEDURE getComment;'
                        . ' DROP PROCEDURE updateComment;'
                        . ' DROP PROCEDURE approveComment;'
                        . ' DROP PROCEDURE getArticleApprovedComments;'
                        . ' DROP PROCEDURE unapproveComment;');
	}

}
