<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('url',255);
                    $table->string('description',255)->nullable();
                    $table->boolean('active')->default(1);
                    $table->timestamps();
                    $table->softDeletes();
                });
                
                DB::transaction(function() {
                        //Stored procedures
                    $addImage = <<<SQL
                               CREATE PROCEDURE `addImage`(
                                   IN `url` VARCHAR(255),
                                   IN `description` VARCHAR(255))
                               BEGIN
                                       INSERT INTO `images`
                                           (`url`,`description`,`created_at`) 
                                       VALUES
                                           (url, description, null);
                               END
SQL;
                   $deleteImage = <<<SQL
                               CREATE PROCEDURE `deleteImage`(
                                   IN `image_id` INT(10))
                               BEGIN
                                       UPDATE `images` 
                                       SET `active`= 0
                                       WHERE `id` = image_id;
                               END
SQL;
                   $updateImage = <<<SQL
                               CREATE PROCEDURE `updateImage`(
                                   IN `image_id` INT(10),
                                   IN `url` VARCHAR(255),
                                   IN `description` VARCHAR(255))
                               BEGIN
                                       UPDATE `images` 
                                       SET 
                                               `url` = url,
                                               `description` = description
                                       WHERE `id`= image_id;
                               END
SQL;
                   $getImage = <<<SQL
                               CREATE PROCEDURE `getImage`(
                                   IN `image_id` INT(10))
                               BEGIN
                                   SELECT 
                                       `id`,
                                       `url`,
                                       `description`,
                                       `created_at`,
                                       `updated_at`
                                   FROM `images`
                                   WHERE `id` = image_id
                                   AND `active` = 1;
                               END
SQL;
                   $getImages = <<<SQL
                               CREATE PROCEDURE `getImages`()
                               BEGIN
                                  SELECT 
                                      `id`,
                                      `url`,
                                      `description`,
                                      `created_at`,
                                      `updated_at`
                                  FROM `images`
                                  WHERE `active` = 1;
                               END
SQL;
                   DB::unprepared($addImage);
                   DB::unprepared($deleteImage);
                   DB::unprepared($updateImage);
                   DB::unprepared($getImage);
                   DB::unprepared($getImages);    
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('images');
                DB::unprepared('DROP PROCEDURE addImage; DROP PROCEDURE deleteImage; DROP PROCEDURE updateImage; DROP PROCEDURE getImage; DROP PROCEDURE getImages;');
	}

}
