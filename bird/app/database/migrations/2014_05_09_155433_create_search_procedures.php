<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchProcedures extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
                DB::transaction(function () {
                    
                    $search = <<<SQL
                            CREATE PROCEDURE `search`(
                                IN `search` VARCHAR(255))
                            BEGIN
                                SELECT 
                                    `id`,
                                    `title`,
                                    substring(`content`,0,120) AS summary,
                                    `description`,
                                    `articles`.`created_at`,
                                    `articles`.`section_id`
                                FROM `articles`
                                WHERE 
                                    `title` LIKE CONCAT('%',search,'%')
                                    OR 
                                    `content` LIKE CONCAT('%',search,'%')
                                    OR 
                                    `description` LIKE CONCAT('%',search,'%')
                                    AND `active` = 1;
                            END
SQL;
                    $searchByTitle = <<<SQL
                            CREATE PROCEDURE `searchByTitle`(
                                IN `search` VARCHAR(255))
                                BEGIN
                                    SELECT 
                                        `id`,
                                        `title`,
                                        substring(`content`,0,120) AS summary,
                                        `description`,
                                        `articles`.`created_at`,
                                        `articles`.`section_id`
                                    FROM `articles`
                                    WHERE 
                                        `title` LIKE CONCAT('%',search,'%')
                                        AND `active` = 1;
                                END
SQL;
                    $searchByDate = <<<SQL
                            CREATE PROCEDURE `searchByDate`(
                                IN `search` DATE)
                            BEGIN
                                SELECT 
                                    `id`,
                                    `title`,
                                    substring(`content`,0,120) AS summary,
                                    `description`,
                                    `articles`.`created_at`,
                                    `articles`.`section_id`
                                FROM `articles`
                                WHERE 
                                    DATE(`created_at`) = search
                                    AND `active` = 1;
                            END
SQL;
                    $searchByDateRange = <<<SQL
                            CREATE PROCEDURE `searchByDateRange`(
                                IN `search1` DATE,
                                IN `search2` DATE)
                            BEGIN
                                SELECT 
                                    `id`,
                                    `title`,
                                    substring(`content`,0,120) AS summary,
                                    `description`,
                                    `articles`.`created_at`,
                                    `articles`.`section_id`
                                FROM `articles`
                                WHERE 
                                    DATE(`created_at`) BETWEEN `search1` AND `search2`
                                    AND `active` = 1;
                            END
SQL;
                    DB::unprepared($search);
                    DB::unprepared($searchByTitle);
                    DB::unprepared($searchByDate);
                    DB::unprepared($searchByDateRange);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::unprepared('DROP PROCEDURE search;'
                        . ' DROP PROCEDURE searchByTitle;'
                        . ' DROP PROCEDURE searchByDate;'
                        . ' DROP PROCEDURE searchByDateRange;');
	}

}
