<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permissions', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('name',20);
                    $table->string('access',255);
                    $table->string('description',255);
                    $table->timestamps();
                });
                
                DB::transaction(function () {
                    $addPermission = <<<SQL
                                CREATE PROCEDURE `addPermission`(
                                    IN `name` VARCHAR(20),
                                    IN `access` VARCHAR(255),
                                    IN `description` VARCHAR(255))
                                    BEGIN
                                        INSERT INTO `permissions`
                                        (`name`, `access`, `description`)
                                        VALUES
                                        (name, access, description);
                                    END
SQL;
                    $updatePermission = <<<SQL
                                CREATE PROCEDURE `updatePermission`(
                                    IN `permission_id` INT(10),
                                    IN `name` VARCHAR(20),
                                    IN `access` VARCHAR(255),
                                    IN `description` VARCHAR(255))
                                BEGIN
                                    UPDATE `permissions`
                                    SET 
                                        `name` = name,
                                        `access` = access,
                                        `description` = description
                                    WHERE `id` = permission_id;
                                END
SQL;
                    $getPermissions = <<<SQL
                                CREATE PROCEDURE `getPermissions`()
                                    BEGIN
                                        SELECT `permissions`.`id`,
                                        `permissions`.`name`,
                                        `permissions`.`access`,
                                        `permissions`.`description`
                                            FROM `permissions`;
                                    END
SQL;
                    $getPermission = <<<SQL
                                CREATE PROCEDURE `getPermission`(
                                    IN `permission_id` INT(10))
                                BEGIN
                                    SELECT
                                        `permissions`.`id`,
                                        `permissions`.`name`,
                                        `permissions`.`access`,
                                        `permissions`.`description`
                                    FROM `permissions`
                                    WHERE `permissions`.`id` = permission_id;
                                END
SQL;

                    DB::unprepared($addPermission);
                    DB::unprepared($getPermission);
                    DB::unprepared($getPermissions);
                    DB::unprepared($updatePermission);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('permissions');
                DB::unprepared('DROP PROCEDURE addPermission;'
                        . 'DROP PROCEDURE getPermission;'
                        . 'DROP PROCEDURE getPermissions;'
                        . 'DROP PROCEDURE updatePermission;');
	}

}
