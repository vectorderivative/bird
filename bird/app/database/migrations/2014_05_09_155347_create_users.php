<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('username',35)->unique();
                    $table->string('password',32);
                    $table->string('email',50)->unique();
                    $table->string('name',25);
                    $table->string('lastname',25);
                    $table->bigInteger('telephone')->default(0);
                    $table->binary('avatar')->nullable();
                    $table->boolean('active')->default(1);
                    $table->timestamps();//Agrega created_at & updated_at
                    $table->softDeletes();
                });
                
                DB::transaction(function () {
                    $SQLTrigger = <<<SQL
                            CREATE TRIGGER onDeleteUser AFTER UPDATE ON users 
                                FOR EACH ROW
                                    IF (NEW.active <> OLD.active AND NEW.active = 0) THEN
                                        UPDATE `articles` SET `active`= 0 WHERE user_id = NEW.id;
                                    END IF
SQL;
                    DB::unprepared($SQLTrigger);

                    //Stored Procedures
                    $addUser = <<<SQL
                                CREATE PROCEDURE `addUser`(
                                    IN `username` VARCHAR(35),
                                    IN `password` VARCHAR(32),
                                    IN `email` VARCHAR(50))
                                        BEGIN
                                            INSERT INTO `users` (
                                                `username`,
                                                `password`,
                                                `email`,
                                                `created_at`)
                                            VALUES (username, MD5(password), email, null);
                                END
SQL;
                    $deleteUser = <<<SQL
                                CREATE PROCEDURE `deleteUser`(
                                    IN `user_id` INT(10))
                                        BEGIN
                                            UPDATE `users`
                                            SET `active`= 0
                                            WHERE `id`=user_id;
                                END
SQL;
                    $updateUser = <<<SQL
                                CREATE PROCEDURE `updateUser`(
                                    IN `user_id` INT(10),
                                    IN `username` VARCHAR(35),
                                    IN `password` VARCHAR(32),
                                    IN `email`VARCHAR(50),
                                    IN `name` VARCHAR(25),
                                    IN `lastname` VARCHAR(25),
                                    IN `telephone` bigint(19))
                                        BEGIN
                                            /*SET @password = (SELECT `password` FROM `users` WHERE `users`.`id` = user_id);*/
                                            UPDATE `users` 
                                            SET 
                                                `username` = username,
                                                `password` = IF (password IS NULL OR password = '', (SELECT `password` FROM `users` WHERE `users`.`id` = user_id), MD5(password)),
                                                `email` = email,
                                                `name` = name,
                                                `lastname` = lastname,
                                                `telephone` = telephone
                                           WHERE `id`=user_id;
                                END
SQL;
                    $updateUserAvatar = <<<SQL
                                CREATE PROCEDURE `updateUserAvatar`(
                                    IN `user_id` INT(10),
                                    IN `avatar`  BLOB)
                                    BEGIN
                                        UPDATE `users` 
                                        SET `avatar` = avatar
                                        WHERE `id`=user_id;
                                END
SQL;
                    $getUser = <<<SQL
                                CREATE PROCEDURE `getUser`(
                                    IN `user_id` INT(10))
                                    BEGIN
                                        SELECT
                                            `id`,
                                            `username`,
                                            `password`,
                                            `email`,
                                            `name`,
                                            `lastname`,
                                            `telephone`,
                                            `avatar`,
                                            `created_at`,
                                            `updated_at` 
                                        FROM `users`
                                        WHERE `id` = user_id
                                        AND `active` = 1;
                                    END
SQL;
                    $getUsers = <<<SQL
                                CREATE PROCEDURE `getUsers`()
                                    BEGIN
                                        SELECT 
                                            `id`,
                                            `username`,
                                            `password`,
                                            `email`,
                                            `name`,
                                            `lastname`,
                                            `telephone`,
                                            `avatar`,
                                            `created_at`,
                                            `updated_at` 
                                        FROM `users`
                                        WHERE `active` = 1;
                                    END
SQL;
                    $login = <<<SQL
                                CREATE PROCEDURE `login`(
                                    IN user VARCHAR(35),
                                    IN pass VARCHAR(32))
                                BEGIN
                                        SELECT 
                                            `users`.`id`,
                                            `users`.`username`,
                                            `users`.`email`,
                                            `users`.`name`,
                                            `users`.`lastname`,
                                            `users`.`telephone`,
                                            `users`.`avatar`,
                                            GROUP_CONCAT(`permissions`.`access`) AS access
                                        FROM `users`
                                        JOIN `user_permissions` ON `users`.`id` = `user_permissions`.`user_id`
                                        JOIN `permissions` ON `user_permissions`.`permission_id` = `permissions`.`id`
                                        WHERE `username` = user AND `password` = MD5(pass) AND `active` = 1;
                                END
SQL;

                    //Execute SP creation
                    DB::unprepared($addUser);
                    DB::unprepared($deleteUser);
                    DB::unprepared($updateUser);
                    DB::unprepared($updateUserAvatar);
                    DB::unprepared($getUser);
                    DB::unprepared($getUsers);
                    DB::unprepared($login);
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                DB::unprepared('DROP TRIGGER onDeleteUser');
		Schema::dropIfExists('users');
                DB::unprepared('DROP PROCEDURE addUser; DROP PROCEDURE deleteUser; DROP PROCEDURE updateUser; DROP PROCEDURE updateUserAvatar; DROP PROCEDURE getUser; DROP PROCEDURE getUsers; DROP PROCEDURE login;');
	}

}
