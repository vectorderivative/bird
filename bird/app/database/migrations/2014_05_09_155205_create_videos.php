<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Connection;

class CreateVideos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('videos', function($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->string('url',255);
                    $table->string('description',255)->nullable();
                    $table->boolean('active')->default(1);
                    $table->timestamps();
                    $table->softDeletes();
                });
                
            DB::transaction(function () {
		//Stored procedures
                $addVideo = <<<SQL
                           CREATE PROCEDURE `addVideo`(IN `url` VARCHAR(255), IN `description` VARCHAR(255))
                           BEGIN
                                   INSERT INTO `videos`
                                       (`url`,`description`,`created_at`) 
                                   VALUES
                                       (url, description, null);
                           END
SQL;
                $deleteVideo = <<<SQL
                           CREATE PROCEDURE `deleteVideo`(
                               IN `video_id` INT(10))
                           BEGIN
                                   UPDATE `videos` 
                                   SET `active`= 0
                                   WHERE `id` = video_id;
                           END
SQL;
               $updateVideo = <<<SQL
                           CREATE PROCEDURE `updateVideo`(
                               IN `video_id` INT(10),
                               IN `url` VARCHAR(255),
                               IN `description` VARCHAR(255))
                           BEGIN
                                   UPDATE `videos` 
                                   SET 
                                           `url` = url,
                                           `description` = description
                                   WHERE `id`= video_id;
                           END
SQL;
               $getVideo = <<<SQL
                           CREATE PROCEDURE `getVideo`(
                               IN `video_id` INT(10))
                           BEGIN
                               SELECT
                                    `id`,
                                   `url`,
                                   `description`,
                                   `created_at`,
                                   `updated_at`
                               FROM `videos`
                               WHERE `id` = video_id
                               AND `active` = 1;
                           END
SQL;
               $getVideos = <<<SQL
                           CREATE PROCEDURE `getVideos`()
                           BEGIN
                              SELECT 
                                  `id`,
                                  `url`,
                                  `description`,
                                  `created_at`,
                                  `updated_at`
                              FROM `videos`
                              WHERE `active` = 1;
                           END
SQL;
                DB::unprepared($addVideo);
                DB::unprepared($deleteVideo);
                DB::unprepared($getVideo);
                DB::unprepared($getVideos);
                DB::unprepared($updateVideo);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('videos');
                DB::unprepared('DROP PROCEDURE addVideo; DROP PROCEDURE deleteVideo; DROP PROCEDURE updateVideo; DROP PROCEDURE getVideo; DROP PROCEDURE getVideos;');
	}

}
