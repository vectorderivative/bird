<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PermissionTableSeeder');
                $this->call('UserTableSeeder');
                $this->call('UserPermissionsTableSeeder');
                $this->call('SectionTableSeeder');
                $this->call('ArticleTableSeeder');
                $this->call('CommentTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'username' => 'admin',
            'password' => MD5('admin'),
            'email' => 'admin@admin.com',
            'name' => 'admin',
            'lastname' => 'adminguez',
            'telephone' => 123456,
            ));
        
        User::create(array(
            'username' => 'reportero',
            'password' => MD5('reportero'),
            'email' => 'reportero@reportero.com',
            'name' => 'reportero',
            'lastname' => 'reporteropez',
            'telephone' => 78910,
            ));
        
        User::create(array(
            'username' => 'usuario',
            'password' => MD5('usuario'),
            'email' => 'usuario@usuario.com',
            'name' => 'usuario',
            'lastname' => 'usuarioguez',
            'telephone' => 78910,
            ));
    }

}

class ArticleTableSeeder extends Seeder {

    public function run()
    {
        DB::table('articles')->delete();

        Article::create(array(
            'title' => 'Dummy',
            'content' => 'Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor',
            'description' => 'Dummy description',
            'published' => 1,
            'section_id' => 1,
            'user_id' => 1));
        
        Article::create(array(
            'title' => 'Dummy 2',
            'content' => 'Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor',
            'description' => 'Dummy 2 description',
            'published' => 0,
            'section_id' => 1,
            'user_id' => 1));
    }

}

class SectionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sections')->delete();

        Section::create(array
            ('name' => 'Dummy Section',
            'description' => 'Dummy section',
            'position' => 1));
        
        Section::create(array
            ('name' => 'Dummy 2 Section',
            'description' => 'Dummy section',
            'position' => 2));
    }

}

class CommentTableSeeder extends Seeder {

    public function run()
    {
        DB::table('comments')->delete();

        Comment::create(array(
            'comment' => 'Dummy text Lorem ipsum blah',
            'username' => 'Fulano',
            'email' => 'yeah@yeh.com',
            'approved' => 1,
            'article_id' => 1));
        
        Comment::create(array(
            'comment' => 'Dummy 2 text Lorem ipsum blah',
            'approved' => 1,
            'article_id' => 1,
            'user_id' => 1));
        
        Comment::create(array(
            'comment' => 'Dummy 3 text Lorem ipsum blah',
            'approved' => 0,
            'article_id' => 1,
            'user_id' => 1));
        
        Comment::create(array(
            'comment' => 'Respuesta a comentario',
            'comment_parent' => 1,
            'approved' => 1,
            'article_id' => 1,
            'user_id' => 2));
    }

}

class PermissionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('permissions')->delete();

        Permission::create(array(
            'name' => 'administrator',
            'access' => 'all',
            'description' => 'Adminstrador'));
        
        Permission::create(array(
            'name' => 'creator',
            'access' => 'create',
            'description' => 'Reportero'));
        
        Permission::create(array(
            'name' => 'reader',
            'access' => 'view',
            'description' => 'Usuario'));
    }

}

class UserPermissionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('user_permissions')->delete();

        UserPermissions::create(array(
            'user_id' => 1,
            'permission_id' => 1));
        
        UserPermissions::create(array(
            'user_id' => 2,
            'permission_id' => 2));
        
        UserPermissions::create(array(
            'user_id' => 3,
            'permission_id' => 3));
    }

}