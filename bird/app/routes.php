<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//VISTAS
Route::get('/', 'HomeController@render');

Route::get('section', array(
    'as' => 'sections',
    'uses' => 'SectionController@render'
));

Route::get('section/{id}', array(
    'as' => 'section',
    'uses' => 'SectionController@showSection'
));

Route::get('article/{id}', array(
    'as' => 'article',
    'uses' => 'ArticleController@showArticle'
));

Route::get('admin', 'AdminController@render');

Route::get('contacto', 'PageController@renderContact');

Route::get('nosotros','PageController@renderAbout');


Route::get('search', array(
    'as' => 'searchPage',
    'uses' => 'SearchController@render'
));


//FORMS
Route::post('section', 'SectionController@sortBy');
Route::get('section/{id}/{sort}', 'SectionController@sortBy');


Route::get('search/{q}', array(
    'as' => 'generalSearch',
    'uses' => 'SearchController@search'
));

Route::get('search/{type}/{params}/{q}', array(
    'as' => 'constrainedSearch',
    'uses' => 'SearchController@constrainedSearch'
));

//Services
Route::post('like', 'LikeController@like');

Route::post('dislike', 'LikeController@dislike');

Route::post('comment', 'CommentController@postComment');

Route::post('deleteComment', 'CommentController@deleteComment');