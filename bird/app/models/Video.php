<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Video extends Eloquent {
    protected $table = 'videos';
    protected $guarded = array('id');
    
    public function addVideo() {
        return DB::statement('CALL addVideo(?,?)', 
                array($this->url,
                    $this->description));
    }
    
    public function deleteVideo() {
        if (func_num_args() == 0) {
            return DB::statement('CALL deleteVideo(?)', array($this->id));
        }else{
            return DB::statement('CALL deleteVideo(?)', array(func_get_arg(0)));
        }
    }
    
    public function updateVideo() {
        return DB::statement('CALL updateVideo(?,?,?)', 
                array($this->id,
                    $this->url,
                    $this->description));
    }
    
    public function getVideo($id) {
        return DB::select('CALL getVideo(?)', array($id));
    }
    
    public function getVideos() {
        return DB::select('CALL getVideos() ');
    }
}
