<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Permission extends Eloquent {
    protected $table = 'permissions';
    protected $guarded = array('id');
    
    public function addPermission() {
        return DB::statement('CALL addPermission(?,?,?)', 
                array($this->name,
                    $this->access,
                    $this->description));
    }
    
    public function updatePermission() {
        return DB::statement('CALL updatePermission(?,?,?,?)',
                array($this->id,
                    $this->name,
                    $this->access,
                    $this->description));
    }
    
    public function getPermission($id) {
        return DB::select('CALL getPermission(?)', array($id));
    }
    
    public function getPermissions() {
        return DB::select('CALL getPermissions()');
    }
}
