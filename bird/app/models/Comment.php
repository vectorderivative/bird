<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Comment extends Eloquent {
    protected $table = 'comments';
    protected $guarded = array('id');
    
    public function user() {
        return $this->belongsTo('User');
    }
    
    public function article() {
        return $this->belongsTo('Article');
    }
    
    public function commnentParent() {
        return $this->belongsTo('Comment');
    }
    
    public function addComment() {
        return DB::statement('CALL addComment(?,?,?,?,?,?)', 
                array($this->comment,
                    $this->article()->id,
                    $this->user()->id,
                    $this->commentParent()->id,
                    $this->username,
                    $this->email));
    }
    
    public function deleteComment() {
        if (func_num_args() == 0) {
            return DB::statement('CALL deleteComment(?)', array($this->id));
        }else{
            return DB::statement('CALL deleteComment(?)', array(func_get_arg(0)));
        }
    }
    
    public function getComment($id) {
        return DB::select('CALL getComment(?)', array($id));
    }
    
    public function getArticleComments($articleId) {
        return DB::select('CALL getArticleComments(?)', array($articleId));
    }
    
    public function updateComment() {
        return DB::statement('CALL updateComment(?,?)', 
                array($this->id,
                    $this->comment));
    }
    
    public function approveComment() {
        if (func_num_args() == 0) {
            return DB::statement('CALL approveComment(?)', array($this->id));
        }else{
            return DB::statement('CALL approveComment(?)', array(func_get_arg(0)));
        }
    }
    
    public function unapproveComment() {
        if (func_num_args() == 0) {
            return DB::statement('CALL unapproveComment(?)', array($this->id));
        }else{
            return DB::statement('CALL unapproveComment(?)', array(func_get_args(0)));
        }
    }
    
    public function getArticleApprovedComments($articleId) {
        return DB::select('CALL getArticleApprovedComments(?)', array($articleId));
    }
}
