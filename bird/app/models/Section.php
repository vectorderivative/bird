<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Section extends Eloquent {
    protected $table = 'sections';
    protected $guarded = array('id');
    
    public function addSection() {
        return DB::statement('CALL addSection(?,?,?)', 
                array($this->name,
                    $this->description,
                    $this->position));
    }
    
    public function deleteSection() {
        if (func_num_args()==0) {
            return DB::statement('CALL deleteSection(?)', array($this->id));
        }else{
            return DB::statement('CALL deleteSection(?)', array(func_get_arg(0)));
        }
    }
    
    public function updateSection() {
        return DB::statement('CALL updateSection(?,?,?,?)', 
                array($this->id,
                    $this->name,
                    $this->description,
                    $this->position));
    }
    
    public function getSection($id) {
        return DB::select('CALL getSection(?)', array($id));
    }
    
    public function getSections() {
        return DB::select('CALL getSections()');
    }
}

