<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Image extends Eloquent {
    protected $table = 'images';
    protected $guarded = array('id');
    
    public function addImage() {
        return DB::statement('CALL addImage(?,?)', 
                array($this->url,
                    $this->description));
    }
    
    public function deleteImage() {
        if (func_num_args() == 0) {
            return DB::statement('CALL deleteImage(?)', array($this->id));
        }else{
            return DB::statement('CALL deleteImage(?)', array(func_get_arg(0)));
        }
    }
    
    public function updateImage() {
        return DB::statement('CALL updateImage(?,?,?)', 
                array($this->id,
                    $this->url,
                    $this->description));
    }
    
    public function getImage($id) {
        return DB::select('CALL getImage(?)', array($id));
    }
    
    public function getImages() {
        return DB::select('CALL getImages() ');
    }
}