<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        
        
        protected $guarded = array('id');
        
        
        
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
        
        public function addUser() {
            return DB::statement('CALL addUser(?,?,?)', 
                    array($this->username,
                        $this->password,
                        $this->email));
        }
        
        public function deleteUser() {
            if (func_num_args() == 0) {
                return DB::statement('CALL deleteUser(?)', array($this->id));
            }else{
                return DB::statement('CALL deleteUser(?)', array(func_get_arg(0)));
            }
        }
        
        public function updateUser() {
            return DB::statement('CALL updateUser(?,?,?,?,?,?,?)', 
                    array($this->id, 
                        $this->username, 
                        $this->password, 
                        $this->email, 
                        $this->name, 
                        $this->lastname, 
                        $this->telephone));
        }
        
        public function updateUserAvatar() {
            return DB::statement('CALL updateUserAvatar(?,?)', 
                    array($this->id,
                        $this->avatar));
        }
        
        public function getUser($id) {
            return DB::select('CALL getUser(?)', array($id));
        }
        
        public function getUsers() {
            return DB::select('CALL getUsers()');
        }
        
        /*user & password*/
        public function login() {
            if (func_num_args() == 0) {
                return DB::select('CALL login(?,?)',
                        array($this->username,
                            $this->password));
            }else{
                return DB::select('CALL login(?,?)',array(func_get_arg(0), func_get_arg(1)));
            }
        }
}
