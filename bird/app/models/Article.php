<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class Article extends Eloquent {
    
    protected $table = 'articles';
    protected $guarded = 'id';
    
    public function section() {
        return $this->belongsTo('Section');
    }
    
    public function user() {
        return $this->belongsTo('User');
    }
    
    public function thumbnail() {
        return $this->belongsTo('Image', 'thumbnail');
    }
    
    public function bigPicture() {
        $this->belongsTo('Image', 'bigPicture');
    }
    
    public function addArticle() {
        return DB::statement('CALL addArticle(?,?,?,?,?,?)', 
                array($this->title,
                    $this->content,
                    $this->description,
                    $this->thumbnail()->id,
                    $this->section()->id,
                    $this->user()->id
                ));
    }
    
    public function deleteArticle() {
        if (func_num_args() == 0) {
            return DB::statement('CALL deleteArticle(?)', array($this->id));
        }else{
            return DB::statement('CALL deleteArticle(?)', array(func_get_arg(0)));
        }
    }
    
    public function dislikeArticle() {
        if (func_num_args() == 0) {
            return DB::statement('CALL dislikeArticle(?)', array($this->id));
        }else{
            return DB::statement('CALL dislikeArticle(?)', array(func_get_arg(0)));
        }
    }
    
    public function getArticle($id) {
        return DB::select('CALL getArticle(?)', array($id));
    }
    
    public function getArticles() {
        return DB::select('CALL getArticles()');
    }
    
    public function getSectionArticles($sectionId) {
        return DB::select('CALL getSectionArticles(?)', array($sectionId));
    }
    
    public function getSectionArticlesByComments($sectionId) {
        return DB::select('CALL getSectionArticlesByComments(?)', array($sectionId));
    }
    
    public function getSectionArticlesByLikes($sectionId) {
        return DB::select('CALL getSectionArticlesByLikes(?)', array($sectionId));
    }
    
    public function getSectionArticlesByViews($sectionId) {
        return DB::select('CALL getSectionArticlesByViews(?)', array($sectionId));
    }
    
    public function getSectionXArticles($sectionId,$limite) {
        return DB::select('CALL getSectionXArticles(?,?)', array($sectionId,$limite));
    }
    
    public function getSliderArticles() {
        return DB::select('CALL getSliderArticles()');
    }
    
    public function getUserArticles($userId) {
        return DB::select('CALL getUserArticles(?)', array($userId));
    }
    
    public function likeArticle() {
        if (func_num_args() == 0) {
            return DB::statement('CALL likeArticle(?)', array($this->id));
        }else{
            return DB::statement('CALL likeArticle(?)', array(func_get_arg(0)));
        }
    }
    public function readArticle() {
        if (func_num_args() == 0) {
            return DB::statement('CALL readArticle(?)', array($this->id));
        }else{
            return DB::statement('CALL readArticle(?)', array(func_get_arg(0)));
        }
    }
    
    public function updateArticle() {
        return DB::statement('CALL updateArticle(?,?,?,?,?,?,?)', 
                array($this->id,
                    $this->title,
                    $this->content,
                    $this->description,
                    $this->thumbnail()->id,
                    $this->section()->id,
                    $this->user()->id));
    }
}
