<?php

/* 
 * David Chavez - 2014
 * 1569682
 */
Class UserPermissions extends Eloquent {
    protected $table = 'user_permissions';
    protected $guarded = array('id');
    
    public function user() {
        return $this->belongsTo('User');
    }
    
    public function permission() {
        return $this->belongsTo('Permission');
    }
    
    public function setPermission() {
        return DB::statement('CALL setPermission(?,?)', 
                array($this->user()->id,
                    $this->permission()->id));
    }
    
    public function  unsetPermission() {
        if (func_num_args() == 0) {
            return DB::statement('CALL unsetPermission(?)', array($this->id));
        }else{
            return DB::statement('CALL unsetPermission(?)', array(func_get_arg(0)));
        }
    }
    
    public function getUserPermissions($id) {
        return DB::select('CALL getUserPermissions(?)', array($id));
    }
    
    public function getUsersPermissions() {
        return DB::select('CALL getUsersPermissions()');
    }
}
