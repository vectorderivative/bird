<!-- LOGIN MODAL -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="label-login">Inicia Sesión</h4>
          </div>
      <form class="pure-form pure-form-stacked" action="{{url('/')}}/login" method="post" accept-charset="utf-8">
          <div class="modal-body">
            <fieldset>
                <input class="pure-input-1" type="text" name="user" value="" placeholder="Usuario" required>
                <input class="pure-input-1" type="password" name="password" value="" placeholder="Password" required>
                <input type="hidden" name="type" value="site">
              </fieldset>  
          </div>
          <div class="modal-footer">
            <input type="submit" class="pure-button pure-button-large pure-input-1" value="Entrar">
          </div>
      </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <!-- //LOGIN MODAL -->