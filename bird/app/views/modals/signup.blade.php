    <!-- SIGNUP MODAL -->
    <div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="signup" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="label-registrate">Regístrate</h4>
          </div>
          <form class="pure-form pure-form-stacked" action="<%= request.getServletContext().getContextPath()%>/signup" method="post" accept-charset="utf-8">
            <div class="modal-body">            
                <fieldset>
                    <input class="pure-input-1" type="text" name="username" value="" placeholder="Usuario" required>
                    <input class="pure-input-1" type="email" name="email" value="" placeholder="Email" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
                    <input id="pwd1" class="pure-input-1" type="password" name="password" value="" placeholder="Password" required>
                    <input id="pwd2" class="pure-input-1" type="password" name="password2" value="" placeholder="Verifica password" required>
                </fieldset> 
            </div>
            <div class="modal-footer">
                <input id="register-button" type="submit" class="pure-button pure-button-large pure-input-1" value="Regístrate">
            </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <!-- // SIGNUP MODAL -->
