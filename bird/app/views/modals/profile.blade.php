<% 
                  session = request.getSession();
                  if (session.getAttribute("userInfo") != null) {
                      Users userProfileInfo = (Users) session.getAttribute("userInfo");
                      
    %>
    <!-- PROFILE MODAL -->
    <div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="profile" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="label-profile">Mi perfil: <%= userProfileInfo.getUsername() %></h4>
          </div>
          <form class="pure-form pure-form-stacked" action="<%= request.getServletContext().getContextPath()%>/profile" method="post" accept-charset="utf-8">
            <div class="modal-body">            
                <fieldset>
                   
                    <label>Nombre(s)</label>
                    <input class="pure-input-1" type="text" name="name" value="<%= userProfileInfo.getName() %>">
                    <label>Apellidos(s)</label>
                    <input class="pure-input-1" type="text" name="lastname" value="<%= userProfileInfo.getLastname() %>">
                    <label>Email</label>
                    <input class="pure-input-1" type="email" name="email" value="<%= userProfileInfo.getEmail() %>" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
                    <label>Teléfono</label>
                    <input class="pure-input-1" type="tel" name="telephone" value="<%= userProfileInfo.getTelephone() %>">
                    <label>Password</label>
                    <input type="password" name="password" class="pure-input-1">
                    <small>Al realizar cambios, deberás volver a iniciar sesión*</small>
                </fieldset> 
            </div>
            <div class="modal-footer">
                <input id="register-button" type="submit" class="pure-button pure-button-large pure-input-1" value="Guardar">
                <button class="pure-button pure-button-large pure-button-meh pure-input-1" data-dismiss="modal">Cancelar</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <!-- // PROFILE MODAL -->
    
    <div class="modal fade" id="avatar-upload" tabindex="-1" role="dialog" aria-labelledby="avatar-upload" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="label-comment">Cambiar avatar</h4>
                </div>
                  <form class="pure-form pure-form-stacked" action="<%= request.getServletContext().getContextPath()%>/profile?action=avatar" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                        <div class="modal-body">
                          <fieldset>
                              <input type="file" name="avatar">
                              <input type="hidden" name="id" id="avatar-modal-userid" value="">
                            </fieldset>  
                        </div>
                        <div class="modal-footer">
                          <input type="submit" class="pure-button pure-button-large pure-input-1" value="Enviar">
                        </div>
                    </form>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    </div>
    
    <% } %>