    <!--COMMENT MODAL -->
    <div class="modal fade" id="response" tabindex="-1" role="dialog" aria-labelledby="response" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="label-comment">Responder comentario</h4>
          </div>
      <form class="pure-form pure-form-stacked" action="{{url('/')}}/comment" method="post" accept-charset="utf-8">
          <div class="modal-body">
            <fieldset>
                <% 
                  session = request.getSession();
        
                  if (session.getAttribute("userInfo") == null) {
                %>
                <input class="pure-input-1" type="text" name="username" required placeholder="Usuario/Nombre">
                <input class="pure-input-1" type="email" name="email" required placeholder="E-mail">
                <% } %>
                <textarea name="comment" class="pure-input-1"></textarea>    
                <input type="hidden" name="parentComment" id="response-to-id" value="">
                <input type="hidden" name="articleId" id="comment-modal-article-id" value="">
              </fieldset>  
          </div>
          <div class="modal-footer">
            <input type="submit" class="pure-button pure-button-large pure-input-1" value="Enviar">
          </div>
      </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <!-- //COMMENT MODAL -->