@extends('layouts.master')

@section('header')
    @parent
@stop

@section('main')
            <div class="col-lg-10 col-centered">
                
                @include('layouts.slider')
            
                <div id="toolbar-main" class="row">
                    <time class="col-lg-8">{{ strftime("%A %d - %B - %Y") }}</time>
                    <!--<form class="col-lg-4 sort-articles-form" action="#" method="post" name="sort-articles">
                        <label>
                            <span class="col-lg-6">Ordenar noticia por</span>
                            <span class="col-lg-6">
                                <select class="input">
                                    <option selected="selected">---</option>
                                    <option value=""></option>
                                    <option value="">Blah</option>
                                </select>
                            </span>
                        </label>
                        <input type="submit" class="sr-only" value="Ordenar">
                    </form>-->
                </div>

                <div class="row">

                    <div class="col-lg-3">
                        @include('layouts.sidebar')
                    </div>

                    <section id="destacado" class="col-lg-9">
                        <% if (request.getAttribute("mensaje") != null) { %>
                        <div class="alert alert-danger"><%= request.getAttribute("mensaje")%></div>
                        <% }%>
                        
                        <h1>Lo más destacado</h1>
                        {{r($sections)}}
                        @if (count($sections)>0)
                            @foreach ($sections as $section)
                                
                                @if (count($section['articles'])>0)
                                    <section class="row">
                                        <h2 class="category-header">{{$section['section_name']}}</h2>

                                    @foreach ($section['articles'] as $article)
                                        <article class="article-preview col-lg-5">
                                            <header>
                                                <img src="<%= request.getServletContext().getContextPath()%>/<%= ( Util.theresThumbnail(article) ? article.getThumbnail().getUrl() : "images/250x250.gif" ) %>" class="thumbnail-article-preview img-responsive">
                                                <time>{{$article->created_at}}</time>
                                                <h3 >{{$article->title}}</h3>
                                            </header>
                                            <p>{{$article->description}}</p>
                                            <footer>
                                                <address>
                                                    <small>Por: {{$article->user_name}} {{$article->user_lastname}}</small>
                                                </address>
                                                    <a class="like" data-like="{{$article->id}}"><i class="fa fa-thumbs-o-up fa-2x"></i> <span class="like-count">{{$article->likes}}</span></a>
                                                    <a class="dislike" data-dislike="{{$article->id}}"><i class="fa fa-thumbs-o-down fa-2x"></i> <span class="dislike-count">{{$article->dislikes}}</span></a>
                                                <a class="pure-button pure-button-small" href="{{route('article',$article->id)}}">Leer</a>
                                            </footer>
                                        </article>
                                    @endforeach
                                        <aside class="view-all pull-right col-lg-2">
                                            <p><a href="{{route('section',$section['section_id'])}}">Ver todos</a></p>
                                        </aside>
                                    </section>
                                @endif
                                
                            @endforeach
                        @else    
                                <section class="row">
                                    <div class="alert alert-info">
                                        Nada que mostrar.
                                    </div>
                                </section>
                        @endif
                    </section>

                </div>
            </div>
@stop

@section('footer')
    @parent
@stop

@section('scripts')
    @parent
    @include('modals.login')
    @include('modals.profile')
    @include('modals.signup')
    
        <script type="text/javascript" charset="utf-8">
            $(document).ready(
                function(){
                    $('.carousel').carousel();
                    
                    $('a[data-like]').on('click', function(event) {
                        var $manita = $(this);
                        $.ajax({
                            type: "POST",
                            url: "{{url('/')}}/like",
                            data: { id: $manita.data('like') }
                        }).done(function( response ) {
                            $manita.find('.like-count').text(response.likes);
                            console.log($(this));
                        });
                        event.preventDefault();
                    });
                    
                    $('a[data-dislike]').on('click', function(event) {
                        var $manita = $(this);
                        $.ajax({
                            type: "POST",
                            url: "{{url('/')}}/dislike",
                            data: { id: $manita.data('dislike') }
                        }).done(function( response ) {
                            $manita.find('.dislike-count').text(response.dislikes);
                            console.log($(this));
                        });
                        event.preventDefault();
                    });
                    
                    $('a[data-avatarid]').on('click', function (event) {
                        $userid = $(this);
                        $('#avatar-modal-userid').val($userid.data('avatarid'));
                    });
                });
        </script>
@stop