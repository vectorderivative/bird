@extends('layouts.master')

@section('header')
    @parent
@stop

@section('main')
        <div class="col-lg-10 col-centered">
            
                <div id="toolbar-main" class="row">
                    <time class="col-lg-8">{{ strftime("%A %d - %B - %Y") }}</time>
                    <!--<form class="col-lg-4 sort-articles-form" action="#" method="post" name="sort-articles">
                        <label>
                            <span class="col-lg-6">Ordenar noticia por</span>
                            <span class="col-lg-6">
                                <select class="input">
                                    <option selected="selected">---</option>
                                    <option value=""></option>
                                    <option value="">Blah</option>
                                </select>
                            </span>
                        </label>
                        <input type="submit" class="sr-only" value="Ordenar">
                    </form>-->
                </div>

                <div class="row">

                    <div class="col-lg-3">
                         @include('layouts.sidebar')
                    </div>

                    <section id="destacado" class="col-lg-9">
                        
                        <h1>Manual de usuario</h1>
                        
                        <section class="row">
                            <h2 class="category-header">Portal</h2>
                            <div><p>
                                "Me lo dijo un pajarito" es una aplicación web en PHP.
                                Un portal de noticias con sistema de usuarios.</p>

                                <p>La página principal está dividida en 5 partes</p>

<p>Primero se tiene la cabecera desde la cual se puede acceder a esta sección.
    Dentro de ella a la derecha está el panel para poder loggear y realizar búsquedas.</p>

<p>Inmediatamente abajo se encuentra un slider de imágenes con las últimas 5 noticias.</p>

<p>A la izquierda se encuentra el menu de categorías por las que se puede navegar, siempre está visible.
En la parte central se localizan las últimas 2 noticias de cada categoría.
Al dar click en las manitas se puede indicar si es de su agrado o no el artículo.</p>

<p>Al navegar dentro de una categoría, el manejo es igual que en la principal sólo
que ahora se cuenta con la opción de ordenar las noticias con un select en la esquina
superior derecha.</p>

<p>Al ver el detalle de cada artículo, en la parte derecha se dispone de la galerías multimedia
    de dicho artículo. Se despliega en una ventana modal.</p>

<p>Al fondo se pueden realizar comentarios al respecto.</p>

<p>Se puede reponder a un comentario específico al clickar en el link "Responder" que se encuentra junto al nombre del usuario.</p>
                                
                            </div>
                            
                            
                        </section>
                        <section class="row">
                            <h2 class="category-header">Administración</h2>
                        </section>
                        <div>
                            El area de administración se accede a través de <a href="{{url('/')}} %>/admin">este link</a>
                            Al loguearse se tiene un menu a la izquierda con pocas opciones.
                            <ul>
                                <li>Entradas</li>
                                <li>Categorias</li>
                                <li>Medios</li>
                                <li>Usuarios</li>
                                <li>Permisos</li>
                            </ul>
                            
                            <p>
                                Los menus son muy descriptivos. En entradas se maneja la entrada de noticias.
                                En categorías, las secciones.
                                En medios se administran las imágenes y videos
                                En usuarios se dá de alta usuarios y se les modifica
                                En permisos se les asignan sus roles.
                            </p>
                        </div>
                    </section>

                </div>
            </div>
@stop

@section('footer')
    @parent
@stop

