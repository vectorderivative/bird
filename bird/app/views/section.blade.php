@extends('layouts.master')

@section('header')
@parent
@stop

@section('main') 
    <div class="col-lg-10 col-centered">

        <div id="toolbar-main" class="row">
            <time class="col-lg-8">{{ strftime("%A %d - %B - %Y") }}</time>

            @include('forms.sortArticles')
        </div>

        <div class="row">

            <div class="col-lg-3">
                @include('layouts.sidebar')
            </div>

            <section id="categoria" class="col-lg-9">
                <% if (request.getAttribute("mensaje") != null) { %>
                <div class="alert alert-danger"><%= request.getAttribute("mensaje")%></div>
                <% }%>

                <% 

                @if ($section != null)

                    
                        <h1 class="category-header">{{$section->name}}</h1>
                        
                    @if (count($articles) > 0)
                        
                        <section class="row">
                        @foreach($articles as $article)
                            <article class="article-preview col-lg-4">
                                <header>
                                    <img src="<%= request.getServletContext().getContextPath()%>/<%= ( Util.theresThumbnail(article) ? article.getThumbnail().getUrl() : "images/250x250.gif" ) %>" class="thumbnail-article-preview img-responsive">
                                         <time>{{$article->created_at}}</time>
                                    <h3>{{$article->title}}</h3>
                                </header>
                                <p>{{$article->description}}</p>
                                <footer>
                                    <address>
                                        <small>Por: {{$article->user_name}} {{$article->user_lastname}}</small>
                                    </address>
                                    <a class="like" data-like="{{$article->id}}"><i class="fa fa-thumbs-o-up fa-2x"></i> <span class="like-count">{{$article->likes}}</span></a>
                                    <a class="dislike" data-dislike="{{$article->id}}"><i class="fa fa-thumbs-o-down fa-2x"></i> <span class="dislike-count">{{$article->dislikes}}</span></a>
                                    <a class="pure-button pure-button-small" href="{{route('article',$article->id)}}">Leer</a>
                                </footer>
                            </article>
                        @endforeach
                        </section>
                    
                    @else
                        <p>No hay artículos que mostrar.</p>
                    @endif
                @endif

            </section>

        </div>
    </div>
@stop

@section('footer')
    @parent
@stop

@section('scripts')
    @parent
    @include('modals.login')
    @include('modals.profile')
    @include('modals.signup')
    <script type="text/javascript" charset="utf-8">
    $(document).ready(
            function() {
                $('#sort-type').on('change', function () {
                   $('form[name="sort-articles"]').submit(); 
                });
                
                $('a[data-like]').on('click', function(event) {
                    var $manita = $(this);
                    $.ajax({
                        type: "POST",
                        url: "{{action('LikeController@like')}}",
                        data: {id: $manita.data('like')}
                    }).done(function(response) {
                        $manita.find('.like-count').text(response.likes);
                        console.log($(this));
                    });
                    event.preventDefault();
                });

                $('a[data-dislike]').on('click', function(event) {
                    var $manita = $(this);
                    $.ajax({
                        type: "POST",
                        url: "{{action('LikeController@dislike')}}",
                        data: {id: $manita.data('dislike')}
                    }).done(function(response) {
                        $manita.find('.dislike-count').text(response.dislikes);
                        console.log($(this));
                    });
                    event.preventDefault();
                });
                
                $('a[data-avatarid]').on('click', function(event) {
                    $userid = $(this);
                    $('#avatar-modal-userid').val($userid.data('avatarid'));
                });
            });
</script>
@stop