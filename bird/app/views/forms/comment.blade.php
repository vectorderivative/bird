<form class="pure-form" action="<%= request.getServletContext().getContextPath()%>/comment" method="post" accept-charset="utf-8">
    <fieldset>
        <legend>Deja un comentario</legend>
        <% 
        session = request.getSession();

        if (session.getAttribute("userInfo") == null) {
        %>
        <input class="pure-input-1" type="text" name="username" required placeholder="Usuario/Nombre">
        <input class="pure-input-1" type="email" name="email" required placeholder="E-mail">
        <% } %>
        <textarea class="pure-input-1" name="comment" required></textarea>

        <input class="pure-button pure-button-large pull-right" type="submit" name="enviar" value="Comentar">
        <input type="hidden" id="article-id" name="articleId" value="{{$article->id}}">
    </fieldset>
</form>