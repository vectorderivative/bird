<form class="col-lg-4 sort-articles-form" action="{{action('SectionController@sortBy')}}" method="post" name="sort-articles">
    <label>
        <span class="col-lg-6">Ordenar noticia por</span>
        <span class="col-lg-6">
            <select id="sort-type" name="sort" class="input">
                <option selected="selected">---</option>
                <option value="date">Fecha</option>
                <option value="views">Visitas</option>
                <option value="comments">Comentarios</option>
                <option value="likes">Me gusta</option>
            </select>
        </span>
    </label>
    <input type="hidden" name="id" value="{{$section->id}}">
    <input type="submit" class="sr-only" value="Ordenar">
</form>