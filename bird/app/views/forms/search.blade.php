<form class="pure-form search-bar" action="search" method="get" accept-charset="utf-8">
    <a class="search"><i class="fa fa-search fa-lg"></i></a> <input type="text" name="q" class="search-input">
</form>