@extends('layouts.master')

@section('header')
@parent
@stop

@section('main')
<div class="col-lg-10 col-centered">

    <div class="row">

        <div class="col-lg-3">
            @include('layouts.sidebar')
        </div>

        <section id="noticia" class="col-lg-9">

            <% if (request.getAttribute("mensaje") != null) { %>
            <div class="alert alert-danger"><%= request.getAttribute("mensaje")%></div>
            <% }%>

            <article class="col-lg-10">
                @if ($article)
                <div class="article-image row">
                    <img class="img-responsive" src="<%= request.getServletContext().getContextPath()%>/<%= ( Util.theresThumbnail(article) ? article.getThumbnail().getUrl() : "images/800x350.gif") %>" alt="">
                </div>
                <div class="row article-datebar">
                    <time class="col-lg-6 article-date">{{$article->created_at}}</time>
                    <div class="pull-right article-likes">
                        <a class="like" data-like="{{$article->id}}"><span class="like-count">{{$article->likes}}</span><i class="fa fa-thumbs-o-up fa-2x"></i></a>
                        <a class="dislike" data-dislike="{{$article->id}}"><i class="fa fa-thumbs-o-down fa-2x"></i><span class="dislike-count">{{$article->dislikes}}</span></a>
                    </div>
                </div>
                <div class="clearfix"></div>

                <header class="article-header">
                    <h1>{{$article->title}}</h1>
                    <h2>{{$article->description}}</h2>
                    <address>
                        {{$article->user_name}} {{$article->user_lastname}}
                    </address>
                </header>

                <div class="article-content">
                    <p>{{$article->content}}</p>
                </div>

                <footer>
                    @include('forms.comment')

                    @include('layouts.comments')

                </footer>
            </article>

            <aside class="col-lg-2 multimedia">
                <figure class="photos">
                    <a href="images/1000x600.gif" data-gallery><img class="multimedia-thumbnail" src="images/80x50.gif"></a>
                    <a href="images/800x350.gif" data-gallery><img class="multimedia-thumbnail" src="images/80x50.gif"></a>
                    <a href="images/1000x600.gif" data-gallery><img class="multimedia-thumbnail" src="images/80x50.gif"></a>
                    <a href="images/1000x600.gif" data-gallery><img class="multimedia-thumbnail" src="images/80x50.gif"></a>
                </figure>
                <figure class="video">
                    <a href="http://localhost:8080/birdNews/static/movie.mp4" data-video data-title="videokjjkh" data-type="video/mp4" data-source="http://localhost:8080/birdNews/static/movie.mp4">
                        <span class="video-thumbnail">
                            <i class="fa fa-play-circle fa-2x video-thumbnail-icon"></i><img class="multimedia-thumbnail" src="images/80x50.gif">
                        </span>
                    </a>
                    <a href="http://localhost:8080/birdNews/static/mov_bbb.mp4" data-video data-title="videokjjkh" data-type="video/mp4" data-source="http://localhost:8080/birdNews/static/mov_bbb.mp4">
                        <span class="video-thumbnail">
                            <i class="fa fa-play-circle fa-2x video-thumbnail-icon"></i><img class="multimedia-thumbnail" src="images/80x50.gif">
                        </span>
                    </a>
                </figure>
            </aside>
            @endif
        </section>

    </div>
</div>

@stop

@section('footer')
    @parent
@stop

@section('scripts')
    @parent
    @include('modals.login')
    @include('modals.profile')
    @include('modals.signup')

<script type="text/javascript" charset="utf-8">
    $(document).ready(
            function() {

                $('a[data-like]').on('click', function(event) {
                    var $manita = $(this);
                    $.ajax({
                        type: "POST",
                        url: "{{action('LikeController@like')}}",
                        data: {id: $manita.data('like')}
                    }).done(function(response) {
                        $manita.find('.like-count').text(response.likes);
                        console.log($(this));
                    });
                    event.preventDefault();
                });

                $('a[data-dislike]').on('click', function(event) {
                    var $manita = $(this);
                    $.ajax({
                        type: "POST",
                        url: "{{action('LikeController@dislike')}}",
                        data: {id: $manita.data('dislike')}
                    }).done(function(response) {
                        $manita.find('.dislike-count').text(response.dislikes);
                        console.log($(this));
                    });
                    event.preventDefault();
                });

                $('a[data-avatarid]').on('click', function(event) {
                    $userid = $(this);
                    $('#avatar-modal-userid').val($userid.data('avatarid'));
                });
            });
</script>
@stop