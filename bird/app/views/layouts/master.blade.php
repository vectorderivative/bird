<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Me lo dijo un pajarito</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        {{ stylesheet_link_tag('pure')}}
        {{ stylesheet_link_tag('font-awesome/font-awesome')}}
        {{ stylesheet_link_tag('bootstrap/bootstrap-grid')}}
        {{ stylesheet_link_tag('bootstrap/modal')}}
        {{ stylesheet_link_tag('bootstrap/carousel')}}
        {{ stylesheet_link_tag('blueimp/blueimp-gallery')}}
        {{ stylesheet_link_tag('blueimp/bootstrap-image-gallery')}}
        {{ stylesheet_link_tag('pure/pure-skin')}}
        {{ stylesheet_link_tag('custom/main')}}
    </head>
    <body class="pure-skin-mine">
        <!--[if lt IE 10]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="container-liquid">
            <header class="row" id="header">
                @section('header')
                    @include('layouts.header')
                @show
            </header>

            <main class="row slideRight" id="main">
                @yield('main')
            </main>

            <footer id="footer">
                @section('footer')
                    @include('layouts.footer')
                @show
            </footer>
        </div>

        @section('scripts')
            {{ javascript_include_tag() }}
        @show
    </body>
</html>