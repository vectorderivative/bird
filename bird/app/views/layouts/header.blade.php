            <!--NAVIGATION-->
            <div class="col-md-8 col-lg-10" id="navigation">
                <nav class="pure-menu pure-menu-open pure-menu-horizontal">
                    <a href="{{ URL::to('/') }}" class="pure-menu-heading slideRight"><img src="{{URL::to('/')}}/assets/logo.png"></a>
                    <ul class="slideLeft">
                        <li class="pure-menu-selected"><a href="{{ URL::to('/') }}">Inicio</a></li>
                        <li><a href="{{ URL::to('nosotros') }}">Nosotros</a></li>
                        <li><a href="{{ URL::to('contacto') }}">Contacto</a></li>
                    </ul>
                </nav>
            </div>
            <!--//NAVIGATION-->

            <!--USER PANEL-->
            <div class="col-md-4 col-lg-2 slideLeft" id="login-panel">
                <div class="half-height row">
                    <% 
                        session = request.getSession();
                        if (session.getAttribute("userInfo") != null) {
                            Users userInfo = (Users) session.getAttribute("userInfo");
                    %>
                    
                    <div>
                        <div class="media">
                            <a class="pull-left" data-toggle="modal" data-target="#avatar-upload" data-avatarid="<%= userInfo.getUserId() %>">
                                <% if (userInfo.getAvatar() == null) { %>
                                <img src="{{url()}}/assets/avatar.png" alt="">
                                <% }else{ %>
                                <img width="50" src="<%= bird.controller.Util.PNGDataURI(userInfo.getAvatar()) %>">
                                <% } %>
                            </a>
                            <div class="media-body">
                                <p>Hola, <a data-toggle="modal" data-target="#profile"><%= userInfo.getUsername() %></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <small><a href="<%= request.getServletContext().getContextPath() %>/logout">Cerrar sesión</a></small>
                    </div>
                    
                    <% }else { %>
                    <div>
                        <button class="pure-button pure-button-small" data-toggle="modal" data-target="#login">Inicia sesión</button>
                        <button class="pure-button pure-button-small" data-toggle="modal" data-target="#signup">Regístrate</button>
                    </div>
                    <div>
                        <small>¿Aún no eres miembro?</small>
                    </div>
                    <% } %>
                    
                    
                </div>
                <div class="row search-form">
                    @include('forms.search')
                </div>
            </div>
            <!--//USER PANEL-->