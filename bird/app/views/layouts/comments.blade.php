<aside class="article-comments">
    <ul class="media-list">
    
    @foreach ($comments as $comment)
        <li class="media" id="comment_{{$comment->id}}">
            <article class="comment">
                <a class="pull-left">
                @if ($comment->user_avatar)
                    <img src="data:image/png;base64,{{$comment->user_avatar}}">
                @else
                    <img class="media-object img-rounded" src="{{url()}}/assets/avatar.png">
                @endif
                </a>
                <div class="media-body">
                    <header class="media-heading comment-heading">

                        <h4 class="comment-poster">{{$comment->user_username != null ? $comment->user_username : 'Anónimo'}}</h4>

                        <time class="comment-date">{{$comment->created_at}}</time>
                        <div class="pull-right comment-likes">
                            <a data-answer="{{$comment->id}}">Responder</a>
                            <span>#{{$comment->id}}</span>

                            <% 
                            session = request.getSession();
                            if (session.getAttribute("userInfo") != null) {
                            Users userInfo = (Users) session.getAttribute("userInfo");

                            if (userInfo.getAccess() != null) {
                            if (userInfo.getAccess().indexOf("all") != -1) {
                            %><a data-delete-comment="{{$comment->id}}" class="pure-button pure-button-small pure-button-error"><i class="fa fa-trash-o"></i></a><%
                            }
                            }
                            }
                            %>

                        </div>
                    </header>
                    <p class="comment-content">
                    @if ($comment->comment_parent != null)
                        En respuesta a <a href="#comment_{{$comment->comment_parent}}">#{{$comment->comment_parent}}</a>
                    @endif
                        <br>
                        {{$comment->comment}}
                    </p>
                </div>
            </article>
        </li><hr>
        @endforeach
    </ul>
</aside>