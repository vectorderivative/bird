<menu class="pure-menu pure-menu-open pure-menu-horizontal">
    <ul>
        <li><a href="{{url('/')}}">Inicio</a></li>
        <li><a href="{{url('/')}}/nosotros">Nosotros</a></li>
        <li><a href="{{url('/')}}/contacto">Contacto</a></li>
    </ul>
</menu>