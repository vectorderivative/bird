<% 
    List<Article> sliderArticles = ArticleDAO.getSliderArticles();
    
    if (sliderArticles != null) {
        int i= 0;
%>
<!--CAROUSEL MAIN-->
                <div id="carousel-main" class="carousel slide col-lg-10 col-centered visible-md visible-lg" data-ride="carousel">

                    <div class="carousel-ui">
                     <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <%  
                                i = 0;
                                for (Article sliderArticle : sliderArticles) { %>
                           <li data-target="#carousel-main" data-slide-to="<%= i%>" data-link="show?id=<%= sliderArticle.getArticleId() %> " class="<%= (i==0 ? "active" : " ") %>"></li>
                           <%
                                i++;
                                }
                           %>
                        </ol>
                           <a class="pure-button pure-button-meh pure-button-xlarge carousel-link" href="show?id=<%= sliderArticles.get(0).getArticleId() %> ">Seguir leyendo</a>
                    </div>

                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">
                         
                         <% 
                            i = 0;
                            for (Article sliderArticle : sliderArticles) {
                         %>
                       <figure class="item <%= (i==0 ? "active" : " ") %>">
                         <div class="carousel-image">
                             <%
                             boolean theresThumbnail = Util.theresThumbnail(sliderArticle);
                             
                             %>
                         <img src="{{url('/')}}/<%= ( theresThumbnail ? sliderArticle.getThumbnail().getUrl() : "images/1000x400_slide1.gif") %>" alt="...">
                         </div>
                         <figcaption class="carousel-caption">
                            <h1><%= sliderArticle.getTitle() %></h1>
                            <p><%= sliderArticle.getDescription() %></p>
                         </figcaption>
                       </figure>
                         <% 
                            i++;
                            } 
                         %>
                     </div>

                     <!-- Controls
                     <a class="left carousel-control" href="#carousel-main" data-slide="prev">
                       <span class="fa fa-chevron-left"></span>
                     </a>
                     <a class="right carousel-control" href="#carousel-main" data-slide="next">
                       <span class="fa fa-chevron-right"></span>
                     </a>
                     -->
                </div>
            <!--//CAROUSEL MAIN-->
<% } %>