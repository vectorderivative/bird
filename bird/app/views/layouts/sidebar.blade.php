@if (count($mySectionsList)>0)
                        <menu id="menu-categories" class="pure-menu pure-menu-open">
                            <h5 class="pure-menu-heading">Categorías</h5>
                            <ul>
                                @foreach ($mySectionsList as $section)
                                <li>
                                    <a href="{{route('section', $section->id)}}">
                                        {{$section->name}}
                                    </a></li>
                                @endforeach
                            </ul>
                        </menu>
@else
<menu id="menu-categories" class="pure-menu pure-menu-open">
    <h5 class="pure-menu-heading">Sin categorías</h5>
</menu>
@endif